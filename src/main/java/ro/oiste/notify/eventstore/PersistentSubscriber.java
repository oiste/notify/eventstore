package ro.oiste.notify.eventstore;

import com.eventstore.dbclient.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PersistentSubscriber {
    @Value("${notify.eventstore.connection-string}")
    private String connectionString;
    private PersistentSubscriptionListener persistentSubscriptionListener;
    private String stream;
    private String subscriptionGroup;

    public PersistentSubscriber() {
    }

    public PersistentSubscriber(
            String connectionString,
            PersistentSubscriptionListener persistentSubscriptionListener,
            String stream,
            String subscriptionGroup
    ) {
        this.connectionString = connectionString;
        this.persistentSubscriptionListener = persistentSubscriptionListener;
        this.stream = stream;
        this.subscriptionGroup = subscriptionGroup;
    }

    public void getEventsList() {
        if (null == this.connectionString) {
            return;
        }

        try {
            EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(this.connectionString);

            EventStoreDBPersistentSubscriptionsClient persistentSubClient = EventStoreDBPersistentSubscriptionsClient.createToStream(settings);

            final UserCredentials credentials = new UserCredentials("admin", "changeit");

            SubscribePersistentSubscriptionOptions options = SubscribePersistentSubscriptionOptions.get()
                    .authenticated(credentials);

            persistentSubClient.subscribeToStream(
                    this.stream,
                    this.subscriptionGroup,
                    options,
                    this.persistentSubscriptionListener
            );

//            //Creating a persistent sub
//            persistentSubClient.create("$ce-currency_exchange", "test-sub-1", CreatePersistentSubscriptionOptions.get()
//                            .authenticated(credentials)
//                            .settings(PersistentSubscriptionSettings.builder().build()))
//                    .get();

        } catch (Throwable throwable) {
            if (null != throwable.getCause()) {
                System.out.println(throwable.getCause().getClass());
                System.out.println(throwable.getCause().getMessage());
            }
            System.out.println(throwable.getMessage());
        }
    }
}
