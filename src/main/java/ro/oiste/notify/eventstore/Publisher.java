package ro.oiste.notify.eventstore;

import com.eventstore.dbclient.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.oiste.notify.eventstore.exception.InvalidConnectionStringException;
import ro.oiste.notify.eventstore.model.Event;

import java.util.UUID;

@Component
public class Publisher {
    @Value("${notify.eventstore.connection-string}")
    private String connectionString;

    public Publisher() {
    }

    public void publishEvent(Event event) {
        if (null == this.connectionString) {
            return;
        }

        try {
            EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(this.connectionString);

            EventStoreDBClient client = EventStoreDBClient.create(settings);
            EventData eventData = EventData
                    .builderAsJson(
                            UUID.randomUUID(),
                            event.getType(),
                            event.getDataObject()
                    )
                    .build();

            AppendToStreamOptions options = AppendToStreamOptions.get()
                    .expectedRevision(ExpectedRevision.ANY);

            client.appendToStream(event.getStream(), options, eventData).get();
        } catch (Throwable throwable) {
            if (null != throwable.getCause()) {
                System.out.println(throwable.getCause().getClass());
                System.out.println(throwable.getCause().getMessage());
            }
            System.out.println(throwable.getMessage());
        }
    }
}
