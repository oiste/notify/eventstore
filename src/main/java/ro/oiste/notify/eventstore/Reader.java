package ro.oiste.notify.eventstore;

import com.eventstore.dbclient.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.oiste.notify.eventstore.exception.InvalidConnectionStringException;

import java.util.List;

@Component
public class Reader {
    @Value("${notify.eventstore.connection-string}")
    private String connectionString;
    private List<ResolvedEvent> eventsList = null;

    public Reader() {
    }

    public List<ResolvedEvent> getEventsList(String stream) {
        return getEventsList(0, stream, true);
    }

    public List<ResolvedEvent> getEventsList(String stream, Boolean useCache) {
        return getEventsList(0, stream, useCache);
    }

    public List<ResolvedEvent> getEventsList(long startEventRevision, String stream, Boolean useCache) {
        if (null == this.connectionString) {
            return eventsList;
        }

        try {
            //TODO: expire cache somehow
            if (useCache && null != eventsList && !eventsList.isEmpty()) {
                return eventsList;
            }

            EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(this.connectionString);

            EventStoreDBClient client = EventStoreDBClient.create(settings);

            ReadStreamOptions readStreamOptions = ReadStreamOptions.get()
                    .forwards()
                    .fromRevision(startEventRevision)
                    .resolveLinkTos();

            ReadResult readResult = client
                    .readStream(stream, readStreamOptions)
                    .get();

            eventsList = readResult.getEvents();

            return eventsList;
        } catch (Throwable throwable) {
            if (null != throwable.getCause()) {
                System.out.println(throwable.getCause().getClass());
                System.out.println(throwable.getCause().getMessage());
            }
            System.out.println(throwable.getMessage());

            return eventsList;
        } finally {
            if (!useCache) {
                eventsList = null;
            }
        }
    }
}
