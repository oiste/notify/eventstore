package ro.oiste.notify.eventstore.exception;

public class InvalidConnectionStringException extends Exception {
    public InvalidConnectionStringException(String message) {
        super(message);
    }
}
