package ro.oiste.notify.eventstore.model;

import org.springframework.context.ApplicationEvent;

public class Event extends ApplicationEvent {
    private Object data;
    private String type;
    private String stream;

    static final String STREAM_ID_SEPARATOR = "-";

    public Event(Object source) {
        super(source);
    }

    public Event(Object source, Object data, Type type, String stream, Long id) {
        super(source);
        this.data = data;
        this.type = type.toString();
        this.stream = stream + STREAM_ID_SEPARATOR + id.toString();
    }

    public Event(Object source, Object data, Type type, String stream) {
        super(source);
        this.data = data;
        this.type = type.toString();
        this.stream = stream;
    }

    public Object getDataObject() {
        return data;
    }

    public String getType() {
        return type;
    }

    public String getStream() {
        return stream;
    }
}
