package ro.oiste.notify.eventstore.model;

public enum Type {
    notification_created,
    notification_conditions_met,
    send_email_notification,
    send_sms_notification,
    email_notification_sent
}
