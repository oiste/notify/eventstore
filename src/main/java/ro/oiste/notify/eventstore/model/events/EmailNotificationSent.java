package ro.oiste.notify.eventstore.model.events;

import ro.oiste.notify.eventstore.model.Event;

public class EmailNotificationSent extends Event {
    public EmailNotificationSent(Object source) {
        super(source);
    }

//    public EmailNotificationSent(Object source, EmailNotificationSentDTOInterface emailNotificationDTO) {
//        super(
//                source,
//                emailNotificationDTO.getEventData(),
//                Type.email_notification_sent,
//                emailNotificationDTO.getStream()
//        );
//    }
}
