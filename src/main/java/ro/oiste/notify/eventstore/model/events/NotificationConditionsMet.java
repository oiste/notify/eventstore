package ro.oiste.notify.eventstore.model.events;

import ro.oiste.notify.eventstore.model.Event;

public class NotificationConditionsMet extends Event {
    NotificationConditionsMetDTOInterface notificationConditionsMetDTO;

    public NotificationConditionsMet(Object source) {
        super(source);
    }

    public NotificationConditionsMet(Object source, NotificationConditionsMetDTOInterface notificationConditionsMetDTO) {
        super(source);
        this.notificationConditionsMetDTO = notificationConditionsMetDTO;
    }

    public NotificationConditionsMetDTOInterface getNotificationConditionsMetDTO() {
        return notificationConditionsMetDTO;
    }
}
