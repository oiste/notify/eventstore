package ro.oiste.notify.eventstore.model.events;

import ro.oiste.notify.eventstore.model.events.NotificationsConditionsMet.NotifyVia;
import ro.oiste.notify.eventstore.model.events.NotificationsConditionsMet.Payload;

public interface NotificationConditionsMetDTOInterface {
    public String getStream();
    public void setStream(String stream);
    public Payload getPayload();
    public void setPayload(Payload payload);
    public NotifyVia getNotifyVia();
    public void setNotifyVia(NotifyVia notifyVia);
}
