package ro.oiste.notify.eventstore.model.events.NotificationsConditionsMet;

public class NotifyVia {
    private Long userId;
    private Boolean sms;
    private Boolean email;

    public NotifyVia(Long userId, Boolean sms, Boolean email) {
        this.userId = userId;
        this.sms = sms;
        this.email = email;
    }

    public NotifyVia() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getSms() {
        return sms;
    }

    public void setSms(Boolean sms) {
        this.sms = sms;
    }

    public Boolean getEmail() {
        return email;
    }

    public void setEmail(Boolean email) {
        this.email = email;
    }
}
