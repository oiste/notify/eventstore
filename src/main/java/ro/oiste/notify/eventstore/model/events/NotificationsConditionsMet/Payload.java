package ro.oiste.notify.eventstore.model.events.NotificationsConditionsMet;

public class Payload {
    private Object eventData;
    private Long userId;

    public Payload() {
    }

    public Payload(Object eventData, Long userId) {
        this.eventData = eventData;
        this.userId = userId;
    }

    public Object getEventData() {
        return eventData;
    }

    public void setEventData(Object eventData) {
        this.eventData = eventData;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
