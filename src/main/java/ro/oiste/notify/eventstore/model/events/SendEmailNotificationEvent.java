package ro.oiste.notify.eventstore.model.events;

import ro.oiste.notify.eventstore.model.Event;

public class SendEmailNotificationEvent extends Event {
    SendEmailNotificationEventDTOInterface emailNotificationDTO;

    public SendEmailNotificationEvent(Object source) {
        super(source);
    }

    public SendEmailNotificationEvent(Object source, SendEmailNotificationEventDTOInterface emailNotificationDTO) {
        super(source);
        this.emailNotificationDTO = emailNotificationDTO;
    }

    public SendEmailNotificationEventDTOInterface getEmailNotificationDTO() {
        return emailNotificationDTO;
    }
}
